#!/usr/bin/env node

const fs = require('fs');
var dateFormat = require('dateformat');

let rawdata = fs.readFileSync('zabojniki.json');
let zabojniki = JSON.parse(rawdata);

let out = [];
let base_date = new Date("2020-02-28")
zabojniki["rows"].forEach(element => {
    let zabojnik ={};
    if(element.RAJON.length>0){
        element.RAJON.forEach(bin =>{
            var date = new Date(bin.DATUM);
            var future = new Date(base_date.getTime() + parseInt(bin.FREKVENCA) * 86400000);
            var diff = (future.getTime() -date.getTime())/ (1000 * 3600 * 24); 
            out.push({
                latitude : element.X,
                longitutde : element.Y,
                rfid : element.RFID,
                frequency : parseInt(bin.FREKVENCA),
                name : bin.NAZIV,
                last_pickup : dateFormat(date,"isoDate"),
                next_pickup : dateFormat(future,"isoDate"),
                days_left : parseInt(bin.FREKVENCA) - diff
            });
        });
    }else{
        out.push({
            latitude : element.Y,
            longitutde : element.X,
            rfid : element.RFID,
            frequency : 7,
            name : "NA"
        });
    }
});

fs.writeFileSync('data.json', JSON.stringify(out));

// zabojniki.rows.forEach(source => {
//     zabojniki.rows.forEach(destination => {
//         let url = "http://localhost:5000/route/v1/driving/" + source.X +","+source.Y+
//         ";"+ destination.X +","+destination.Y + "?steps=false";
        
//         http.get(url, res => {
//             res.setEncoding("utf8");
//             let body = "";
//             res.on("data", data => {
//                 body += data;
//             });
//             res.on("end", () => {
//                 body = JSON.parse(body);
//                 console.log(body);
//             });
//         });  
//     });
// });