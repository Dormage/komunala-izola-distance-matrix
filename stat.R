library("rjson")
library(ggplot2)
data <- fromJSON(file="data.json")

ggplot(data=hist, aes(x=days_left, y=freq)) +
  geom_bar(stat="identity", fill="steelblue")+
  geom_text(aes(label=freq), vjust=1.6, color="white", size=3.5)+
  theme_minimal()